<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request ;

class MyMailInquiry extends Mailable
{
    use Queueable, SerializesModels;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
        return $this->view('mymailinquiry',['names'=>$request->names,'emails'=>$request->emails,'phoneNumbers'=>$request->phoneNumbers,'descriptions'=>$request->descriptions,'projectTypes'=>$request->projectTypes])->to('lucoadam@gmail.com')->subject('STS');
    }
}
