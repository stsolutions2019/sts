import React,{Component} from 'react';
import '../../../css/services/eachservicestyle.css';
import { ChevronRight } from 'react-bytesize-icons';
import { NavLink } from 'react-router-dom';
import Modal from 'react-awesome-modal';
import MainFooter from '../FooterComponent/MainFooter.js';

export default class Career extends Component{
    constructor(props){
      super(props);
      this.state={
        visible : false,
        content:[
          {key:1,name:'Android Development',time:'4 year'},
          {key:2,name:'Web Development',time:'3 year'},
          {key:3,name:'Data Analytics',time:'2 year'},
          {key:4,name:'Server Specialist',time:'1 year'},
        ]
      }
      this.eachCareer=this.eachCareer.bind(this);
    }
    openModal() {
        this.setState({
            visible : true
        });
    }

    closeModal() {
        this.setState({
            visible : false
        });
    }

    eachCareer(){
      const contentArray = this.state.content;
      const eachContent = contentArray.map((content)=>{
        return(
          <div key={content.key}className="card mb-2 mr-2" style={{width: '18rem',color:'white',backgroundColor:'rgba(0,0,0,0.4)',backgroundImage:`url(${require('../../../assest/image/career/card.jpg')})`}}>
            <div className="card-body">
              <h5 className="card-title text-center">{content.name}</h5>
              <h6 className="card-subtitle mb-2 text-center">{content.time}</h6>
                <span className="btn btn-success" style={{margin:'0 82.5px'}} onClick={() => this.openModal()}>Apply Now</span>
          </div>
        </div>
      );
      }
      );
      return(
        eachContent
      );
    }
    render(){
      return (
        <div>
          <div className="serviceWrapper">
            <div className="serviceNavBar">
                <div><NavLink to="/">
                  <span className='header'>Company</span>
                </NavLink>  /   <span>Career </span> </div>
            </div>
            <div className="serviceHeader" style={{backgroundPosition:'center',backgroundImage:`url(${require('../../../assest/image/career/background.jpg')})`,backgroundRepeat:'no-repeat',backgroundAttachment:'fixed'}}>
              <span style={{color:'orange'}}>Be a part of our team!</span>
              <span style={{fontSize:'1.2em',color:'white'}}>Our people are our greatest asset.</span>
            </div>
            <div className="serviceContent">
              <span>Recruiting, training, and retaining the best talent, STS has grown from a few to a few hundreds. And the good news is... we haven't stopped yet! There is always room for self-motivated tech enthusiasts at STS.</span>
            </div>

            <div className="serviceSubContentMain">

              <div style={{justifyContent:'center'}} className="serviceSubContent">
                <img style={{width:'90%'}} src={require('../../../assest/image/career/content1.jpg')}/>
              </div>
              <div className="serviceSubContent">
                <div className="serviceSubContentHeader">
                Work Culture
                </div>
                <div className="serviceSubContentContent">
                  <div>A positive and upbeat workplace culture where projects are tackled passionately and employees are treated respectfully—this is what you will experience at STS.</div>

                  <div>We are a flat organization and we strive to keep it that way. Ranks and designations do not stand in the way of innovation, leadership, and individual initiative. Creativity sparks, ideas mature, and solutions evolve in small collaborative teams. Hard work is expected, but we make sure to have fun while we are at it. QBurstians enjoy what they do and so they do it even better.</div>

                  <div>Gear up to be constantly challenged in this fast paced environment that lauds independence, critical thinking, imagination and above all, team spirit.</div>
                </div>
              </div>
              <div className="serviceSubContent">
                <div className="serviceSubContentHeader">
                  Celebrating Life at Work
                </div>
                <div className="serviceSubContentContent">
                  STS is not always work. A host of cultural events spice up our life here. From HacKnights to team outings and sports events, numerous activities draw us out of our regular office routine. The yearly bash that celebrates the spirit of STS tops the list. Our tech geeks break loose and indulge in their cultural talents, putting up entertaining shows for friends and family. Feeling rejuvenated we are back to do what we do best—building game-changing apps!
                </div>
              </div>
              <div style={{justifyContent:'center'}} className="serviceSubContent">
                <img style={{width:'90%'}} src={require('../../../assest/image/career/content2.jpg')}/>
              </div>
              <div className="careerMain">
                <div className="careerHeading">
                    <h3>Join Us</h3>
                </div>
                <div className="careerContent">

                    <this.eachCareer/>
                </div>

              </div>

            </div>
        </div>
        <Modal
                  visible={this.state.visible}
                  width="400"
                  height="300"
                  effect="fadeInUp"
                  onClickAway={() => this.closeModal()}
              >
              <div className="card" style={{width:'400px',height:'auto'}}>
                <div className="card-body">
                  <h4 className="card-title">Submit Data</h4>
                  <form>
                  <div className="form-group">
                    <label for="name">Name</label>
                    <input type="text" className="form-control" id="name"/>
                  </div>
                  <div className="form-group">
                    <label for="email">Email</label>
                    <input type="email" className="form-control" id="email"/>
                  </div>
                <div className="form-group">
                  <label for="filecv">CV</label>
                  <input type="file" className="form-control" id="filecv"/>
                </div>
                <div className="form-group">
                <button type="submit" className="btn btn-success" style={{marginRight:'20px'}}>Submit</button>
                  <span className="btn btn-success" style={{position:'relative',bottom:0}} onClick={() => this.closeModal()}>Close</span>
                  </div>
                  </form>
                </div>
              </div>
         </Modal>
          <div style={{height:'100vh',position:'relative',bottom:0}}>
            <MainFooter/>
          </div>
        </div>
      );
    }
}
