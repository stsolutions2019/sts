import React, { Component } from 'react';
import Fullpage, { FullPageSections, FullpageSection } from '@ap.cx/react-fullpage';
import MessageForm from '../Head/MessageForm/MessageForm.js';
import scrollToComponent from 'react-scroll-to-component';
import { FacebookLoginButton, GoogleLoginButton, TwitterLoginButton, InstagramLoginButton } from "react-social-login-buttons";
import { SocialIcon } from 'react-social-icons';
import Icon from 'react-geomicons';
import { Twitter, Location, Telephone, Message, Mail } from 'react-bytesize-icons';
import MaterialIcon, { colorPalette } from 'material-icons-react';
import MapContainer from '../Head/MapComponent/MapContainer.js';

//animation helper
import posed, { PoseGroup } from "react-pose";
import styled from "styled-components";
import { tween } from "popmotion";

// import '../../../css/header/header.css';


const messageFormStyle = posed.div({
    fullscreen: {
        width: 'fit-content',
        height: 'fit-content',
        bottom: '5%',
        right: '4vw',
        transition: { tween, duration: 1000, type: 'spring', stiffness: 100 }
    },
    idle: {
        width: 'fit-content',
        height: 'fit-content',
        bottom: '-500%',
        right: '4vw',
        transition: tween
    }
});

const StyledMessageForm = styled(messageFormStyle)`
    position: fixed;
    background: rgba(0, 0, 0, 0.4);
    bottom: 5%;
    right: 4vw;
    border-radius: 0 0 40px 0;
    z-index: 99999;
`;

export default class Header extends Component {
    constructor(props) {
        super(props);
        this.handleOnClickPrev = this.handleOnClickPrev.bind(this);
        this.handleOnClickNext = this.handleOnClickNext.bind(this);
        this.updateSlideNumber = this.updateSlideNumber.bind(this);
        this.displayBottomToTopButton = this.displayBottomToTopButton.bind(this);
        this.handleMessageForm = this.handleMessageForm.bind(this);
        this.viewMessageForm = this.viewMessageForm.bind(this);
        this.displayOrHideSocialSite = this.displayOrHideSocialSite.bind(this);
        this.state = {
            isOpen: false,
            prevNext: 0,
            isMiddle: true,
            isTop: true,
            isBottom: true,
            isWhere: true,
            hideOrShowBottomToTop: false,
            displayMessageForm: false,
            active: false,
            change: false,
            hrefGoogle: 'http://www.google.com',
            hrefFacebook: 'http://www.facebook.com',
            hreTwitter: 'http://www.twitter.com',
            hrefInstagram: 'http://www.instagram.com',
            leftBrace: '{',
            rightBrace: '}',
            displaySocialSite: 'block'
        };
    }

    componentDidMount() {
        document.addEventListener('scroll', () => {
            const isTop = window.scrollY < 100;
            if (isTop !== this.state.isTop) {
                this.setState({ isTop })
                // console.log(this.state.isTop);
                this.updateSlideNumber();
            }
        });
        document.addEventListener('scroll', () => {
            const isMiddle = window.scrollY < 800;
            if (isMiddle !== this.state.isMiddle) {
                this.setState({ isMiddle })
                // console.log(this.state.isMiddle);
                this.updateSlideNumber();
            }

        });
        document.addEventListener('scroll', () => {
            const isBottom = window.scrollY < 1500;
            if (isBottom !== this.state.isBottom) {
                this.setState({ isBottom })
                // console.log(this.state.isBottom);
                this.updateSlideNumber();
                this.displayOrHideSocialSite();
            }
        });

    }

    displayOrHideSocialSite() {
        if (this.state.isBottom) {
            this.setState({
                displaySocialSite: 'block'
            })
        } else {
            this.setState({
                displaySocialSite: 'none'
            })
        }
        console.log(this.state.displaySocialSite);
    }

    updateSlideNumber() {
        setTimeout(() => {
            //top of page i.e. page 1
            if (this.state.isTop && this.state.isMiddle && this.state.isBottom) {
                this.setState({
                    prevNext: 0,
                    hideOrShowBottomToTop: false,
                })
                // console.log(this.state.hideOrShowBottomToTop);
            }
            // page 2
            if (!this.state.isTop && this.state.isMiddle && this.state.isBottom) {
                this.setState({
                    prevNext: 1,
                    hideOrShowBottomToTop: false,
                })
                // console.log(this.state.hideOrShowBottomToTop);
            }
            //page3 
            if (!this.state.isTop && !this.state.isMiddle && this.state.isBottom) {
                this.setState({
                    prevNext: 2,
                    hideOrShowBottomToTop: true
                })
                // console.log(this.state.hideOrShowBottomToTop);
            }
            //bottom of page i.e. page 3
            if (!this.state.isTop && !this.state.isMiddle && !this.state.isBottom) {
                this.setState({
                    prevNext: 3,
                    hideOrShowBottomToTop: true
                })
                // console.log(this.state.hideOrShowBottomToTop);
            }
        }, 500)
    }

    displayBottomToTopButton() {
        if (this.state.hideOrShowBottomToTop) {
            return (
                //Note: ##do not delete
                // <div className="_bottomToTop">
                //     <div className="_ToTop" onClick={() => { scrollToComponent(this.Top, { offset: 0, align: 'top', duration: 1500 }) }}>
                //         <img id="arrow-to-top" src={require('../../../assest/arrowIcon/doubleGoToTop.png')}></img>
                //     </div>
                // </div>
                <ScrollButton scrollStepInPx="1000" delayInMs="0" />
            );
        } else {
            return (
                <div className="_bottomToTop" style={{ display: 'none' }}>
                    <div className="_ToTop" onClick={() => { scrollToComponent(this.Top, { offset: 0, align: 'top', duration: 1500 }) }}>
                        <img id="arrow-to-top" src={require('../../../assest/arrowIcon/doubleGoToTop.png')}></img>
                    </div>
                </div>
            )
        }
    }
    //unused function 
    handleOnClickPrev() {
        setTimeout(() => {
            if (this.state.prevNext == 0) {
                //donothing
            } else {
                this.setState({
                    prevNext: --this.state.prevNext
                })
            }
        }, 1000)
        // console.log(this.state.prevNext);
    }
    //unused function
    handleOnClickNext() {
        setTimeout(() => {
            if (this.state.prevNext == 3) {
                //do nothing
            } else {
                this.setState({
                    prevNext: ++this.state.prevNext
                })
            }
        }, 1000)
        // console.log(this.state.prevNext);
    }

    handleMessageForm() {
        this.setState({
            displayMessageForm: !this.state.displayMessageForm,

        })
    }

    changeEmailIcon() {
        this.setState({
            changeEmailIconOrNot: !this.state.changeEmailIconOrNot
        })
    }

    viewMessageForm() {
        if (this.state.displayMessageForm) {
            return (
                <div className="messageForm" >
                    <MessageForm />
                </div>
            )
        } else {
            return (
                <div className="messageForm" style={{ display: 'none' }}>
                    <MessageForm />
                </div>
            )
        }
    }

    render() {
        return (
            <div>
                <div className="sideNextPrev">
                    <div className="_nextPrevContainer">
                        <div className="_Prev" onClick={() => { scrollToComponent(this.Top, { offset: 1, align: 'bottom', duration: 500, ease: 'inCirc' }) }}>
                            <img id="arrow-up" src={require('../../../assest/arrowIcon/singleArrow.png')}></img>
                        </div>
                        <div className="countPage" >
                            <div>{this.state.prevNext + 1}</div>
                            <div>/</div>
                            <div>4</div>
                        </div>
                        <div className="_Next" onClick={() => { scrollToComponent(this.Bottom, { offset: 0, align: 'bottom', duration: 500, ease: 'inCirc' }) }}>
                            <img id="arrow-down" src={require('../../../assest/arrowIcon/singleArrow.png')}></img>
                        </div>
                    </div>
                </div>
                <SocialSite displaySocialSite={this.state.displaySocialSite} />
                {/* <this.viewMessageForm /> */}
                <div className="messageForm" >
                    <StyledMessageForm
                        pose={this.state.active ? "fullscreen" : "idle"}>
                        <MessageForm />
                    </StyledMessageForm>
                </div>

                <div className="emailIcon" onClick={() => { this.setState({ active: !this.state.active }); this.changeEmailIcon(); }} >
                    <Icon id="email-icon" name={this.state.changeEmailIconOrNot ? "close" : "chat"} size='2em' fill="white"></Icon>
                    {/* <SocialIcon id="email-icon" network="email"></SocialIcon> */}
                </div>
                <this.displayBottomToTopButton />
                <Fullpage>
                    <FullPageSections>
                        <FullpageSection className="Top" ref={(FullPageSection) => { this.Top = FullPageSection; }}>

                            <img id="main-image"
                                src={require('../../../assest/image/mainBack.jpg')}></img>
                            <div className="fullPageSectionFirst">
                                <div className="firstSectionText">
                                    Accelerate Your
                                    Digital Journey
                                    with a full-service provider
                                </div>
                            </div>
                        </FullpageSection>
                        <FullpageSection >
                            <div className="fullPageSectionSecond">

                                <div className="card-group-container">
                                    <div style={{ display: 'flex', flexDirection: 'row', textAlign: 'center', alignItems: 'center', justifyContent: 'center' }}>
                                        <h2>Services We Provide</h2>
                                    </div>
                                    <div class="card-group" >
                                        <div class="card">
                                            <div class="card-body text-center"  >
                                                <img src={require('../../../assest/image/Mobile-Application-Development.png')}
                                                    style={{ width: '100%', height: 'auto' }}></img>
                                            </div>
                                        </div>
                                        <div class="card ">
                                            <div class="card-body">
                                                <h2>Mobile Application Development</h2>
                                                <h5>Our company boast of a team of dedicated App developers who help to provide App solutions and services that suits various business requirements and therefore, ensure good results.</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-group" style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap-reverse' }}>
                                        <div class="card ">
                                            <div class="card-body">
                                                <h2>Mobile Application Development</h2>
                                                <h5>Our company boast of a team of dedicated App developers who help to provide App solutions and services that suits various business requirements and therefore, ensure good results.</h5>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-body text-center"  >
                                                <img src={require('../../../assest/image/webApplicationDevelopment.png')}
                                                    style={{ width: '100%', height: 'auto' }}></img>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </FullpageSection>
                        <FullpageSection>
                            <div className="fullPageSectionSecond">
                                <div className="card-group-container" >
                                    <div style={{ display: 'flex', flexDirection: 'row', textAlign: 'center', alignItems: 'center', justifyContent: 'center' }}>
                                        <h2>Our Features</h2>
                                    </div>
                                    <div class="card-group" >
                                        <div class="card">
                                            <div class="card-body text-center"  >
                                                <img src={require('../../../assest/image/Mobile-Application-Development.png')}
                                                    style={{ width: '100%', height: 'auto' }}></img>
                                            </div>
                                        </div>
                                        <div class="card ">
                                            <div class="card-body">
                                                <h2>The Complete Digital Solution</h2>
                                                <h5>QBurst is known for its full-stack development package. With focus teams for every layer of the application—from fluidly responsive screens to powerful data crunching engines and analytical subsystems—we deliver a rock-solid solution.
Our front-end engineers work with the latest JS frameworks and libraries to bring mockups from development to delivery. To build the application brain, skilled backend developers team up with database experts and experienced data scientists. On the cloud or otherwise, we build you resilient systems meshing the various components together with suitable middleware.
For businesses that prefer everything under one roof, we offer design, usability, and other complementary services along with custom application development.</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-group" style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap-reverse' }}>
                                        <div class="card ">
                                            <div class="card-body ">
                                                <h2>Inventive Use of Technology</h2>
                                                <h5>Ethereum smart contracts, AI travel assistants, or virtual reality shopping applications—we enable you to stay ahead of the times through the seamless adoption of technology. Together, let us identify and explore existing and emerging technology to drive innovation.
In an age characterised by digital data deluge and machine intelligence, we help you tap your data. From HTML5 to React and AngularJS, the evolving frontend ecosystem also offers multiple choices. We can help strike a balance between technology and user expectations to build a productive application.</h5>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-body text-center"  >
                                                <img src={require('../../../assest/image/webApplicationDevelopment.png')}
                                                    style={{ width: '100%', height: 'auto' }}></img>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </FullpageSection>
                        <FullpageSection className="Bottom" ref={(FullPageSection) => { this.Bottom = FullPageSection; }}>
                            <MainFooter/>
                        </FullpageSection>
                    </FullPageSections>
                </Fullpage>
            </div>
        );
    }
}

export class MainFooter extends Component {
    constructor(props){
        super(props);
        this.state={
            hrefGoogle: 'http://www.google.com',
            hrefFacebook: 'http://www.facebook.com',
            hreTwitter: 'http://www.twitter.com',
            hrefInstagram: 'http://www.instagram.com',
            leftBrace: '{',
            rightBrace: '}',
        }
    }
    render() {
        return (
            <div className="fullPageSectionFourth">
                {/* <div style={{height:'40px'}}>hello</div> */}
                <div className="sectionFourthContainer">

                    <div className="sectionFourthFirstCol">
                        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                            <img src={require('../../../assest/icon/stsLogo.png')} style={{ width: '65px', height: '65px' }}></img>
                            <div style={{ fontSize: '17px', width: '100%', textAlign: 'center' }}>Sustainable Technological Solutions</div>
                            <div className="imageBorder" ></div>
                        </div>
                        <div style={{ height: '250px', display: 'flex', flexDirection: 'column', justifyContent: 'space-evenly' }}>
                            <div style={{ display: 'flex', flexDirection: 'row', marginTop: '10px' }}>
                                <Telephone></Telephone>
                                <div style={{ marginLeft: '12px' }}>Phone here</div>
                            </div>
                            <div style={{ display: 'flex', flexDirection: 'row' }}>
                                <Mail></Mail>
                                <div style={{ marginLeft: '12px' }}>info@stsolutions.com.np</div>
                            </div>
                            <div style={{ display: 'flex', flexDirection: 'row' }}>
                                <Location></Location>
                                <div style={{ marginLeft: '12px' }}>Location here</div>
                            </div>
                        </div>

                    </div>
                    <div className="sectionFourthFirstCol">
                        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                            <MaterialIcon icon="perm_phone_msg" size="large" color="white"></MaterialIcon>
                            <div style={{ fontSize: '17px' }}>Contact Us</div>
                            <div className="imageBorder" ></div>
                        </div>
                        <div style={{ height: '250px', display: 'flex', flexDirection: 'column', justifyContent: 'space-evenly' }}>
                            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <div>{this.state.leftBrace + 'Manager' + this.state.rightBrace}</div>
                                <div style={{ color: 'yellowgreen' }}>+977-9843563422</div>
                            </div>
                            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <div>{this.state.leftBrace + 'Manager' + this.state.rightBrace}</div>
                                <div style={{ color: 'yellowgreen' }} >+977-9843563422</div>
                            </div>
                            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <div>{this.state.leftBrace + 'Manager' + this.state.rightBrace}</div>
                                <div style={{ color: 'yellowgreen' }}>+977-9843563422</div>
                            </div>
                        </div>

                    </div>
                    <div className="sectionFourthFirstCol">
                        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                            <MaterialIcon icon="location_on" size="large" color="white"></MaterialIcon>
                            <div style={{ fontSize: '17px' }}>Location Map</div>
                            <div className="imageBorder" ></div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'row', background: 'red', width: '100%', height: '250px', position: 'relative' }}>
                            <div style={{ width: '100%', height: '100%', position: 'absolute', borderStyle: 'solid', borderColor: '#9CF5A6', borderWidth: '1px' }} >
                                <MapContainer />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="footer">
                    <div className="textFooter">
                        2019, All Rights Are Reserved By Sustainable Technological Solutions.
                                    </div>
                    <div className="iconFooter">
                        {/* <Twitter className="icon"></Twitter>
                                        <Icon className="icon" name="heart"></Icon>
                                        <Icon className="icon" name="heart"></Icon>
                                        <Icon className="icon" name="heart"></Icon> */}
                        <a href={this.state.hrefFacebook} target="_blank">
                            <SocialIcon className="icon" network="facebook" style={{ width: '30px', height: '30px', color: 'white' }}></SocialIcon>
                        </a>
                        <a href={this.state.hrefGoogle} target="_blank">
                            <SocialIcon className="icon" network="google" style={{ width: '30px', height: '30px' }}></SocialIcon>
                        </a>
                        <a href={this.state.hreTwitter} target="_blank">
                            <SocialIcon className="icon" network="twitter" style={{ width: '30px', height: '30px' }}></SocialIcon>
                        </a>
                        <a href={this.state.hrefInstagram} target="_blank">
                            <SocialIcon className="icon" network="instagram" style={{ width: '30px', height: '30px' }}></SocialIcon>
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}

class SocialSite extends Component {
    constructor() {
        super();
        this.state = {
            btnSize: '35px',
            hrefGoogle: 'http://www.google.com',
            hrefFacebook: 'http://www.facebook.com',
            hreTwitter: 'http://www.twitter.com',
            hrefInstagram: 'http://www.instagram.com',
        }
    }
    render() {
        return (
            <div className="socialSite" style={{ position: "fixed", bottom: '40%', right: '0', zIndex: '9999', display: this.props.displaySocialSite }}>
                <div id="social-site">
                    <a href={this.state.hrefFacebook} target="_blank">
                        {/* <FacebookLoginButton size={this.state.btnSize}  onClick={() => alert("Hello")} >
                            <span>Facebook</span>
                        </FacebookLoginButton> */}
                        <FacebookLoginButton size={this.state.btnSize} >
                            <span>Facebook</span>
                        </FacebookLoginButton>
                    </a>

                </div>
                <div id="social-site">
                    <a href={this.state.hrefGoogle} target="_blank">
                        <GoogleLoginButton size={this.state.btnSize} id="social-site">
                            <span>Google+</span>
                        </GoogleLoginButton>
                    </a>
                </div>
                <div id="social-site">
                    <a href={this.state.hreTwitter} target="_blank">
                        <TwitterLoginButton size={this.state.btnSize} id="social-site" >
                            <span>Twitter</span>
                        </TwitterLoginButton>
                    </a>
                </div>
                <div id="social-site">
                    <a href={this.state.hrefInstagram} target="_blank">
                        <InstagramLoginButton size={this.state.btnSize} id="social-site" >
                            <span>Instagram</span>
                        </InstagramLoginButton>
                    </a>
                </div>
            </div>
        );
    }
}

class ScrollButton extends Component {
    constructor() {
        super();
        this.state = {
            intervalId: 0
        };
    }

    scrollStep() {
        if (window.pageYOffset === 0) {
            clearInterval(this.state.intervalId);
        }
        window.scroll(0, window.pageYOffset - this.props.scrollStepInPx);
    }

    scrollToTop() {
        let intervalId = setInterval(this.scrollStep.bind(this), this.props.delayInMs);
        this.setState({ intervalId: intervalId });
    }

    render() {
        return (
            <div className="_bottomToTop">
                <div className="_ToTop" onClick={() => { this.scrollToTop(); }}>
                    <img id="arrow-to-top" src={require('../../../assest/arrowIcon/doubleGoToTop.png')}></img>
                </div>
            </div>
        );
    }
}