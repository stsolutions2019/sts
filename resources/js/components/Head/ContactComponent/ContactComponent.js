import React, { Component } from 'react';
import MainFooter from '../../FooterComponent/MainFooter.js';
import '../../../../css/contact/contact.css';
import MaterialIcon, { colorPalette } from 'material-icons-react';
import ContactUsForm from '../../ContactUsForm/ContactUsForm';

export default class ContactComponent extends Component {
    constructor(props) {
        super(props);
        this.handleNameChange=this.handleNameChange.bind(this);
        this.handleEmailChange=this.handleEmailChange.bind(this);
        this.handleNumberChange=this.handleNumberChange.bind(this);
        this.handleDescriptionChange=this.handleDescriptionChange.bind(this);
        this.handleProjectTypeChange=this.handleProjectTypeChange.bind(this);
        this.submitInquiry=this.submitInquiry.bind(this);
        this.state = {
            names: '',
            emails:'',
            phoneNumbers:'',
            descriptions:'',
            projectTypes:''
        }
    }

    handleNameChange(e){
        this.setState({
            names:e.target.value
        })
    }

    handleEmailChange(e){
        this.setState({
            emails:e.target.value
        })
    }

    handleNumberChange(e){
        this.setState({
            phoneNumbers: e.target.value
        })
    }

    handleDescriptionChange(e){
        this.setState({
            descriptions: e.target.value
        })
    }

    handleProjectTypeChange(e){
        this.setState({
            projectTypes: e.target.value
        })
    }

    submitInquiry(e) {
        e.preventDefault(); 
        axios.post('/send-inquiry', this.state).then(response => {
            console.log(response);
            if (response.status == 200) {
                // alert('Success! Your detail has been sent. Be patient, we will reply you within 24 hours. Thank You!');
                console.log('success');
            }
        }).then(error => {
            console.log(error);
            // alert('Failed!! due to ' + error);
        });
    } 
    render() {
        return ( 
            <div className="contactMain" >
                <div className="card-deck">
                    <div className="card">
                        <div className="card-body text-center">
                            <div style={{
                                display: 'flex',
                                flexDirection: 'column',
                                marginBottom: '20px'
                            }}>
                                <MaterialIcon icon="mail" size="large" color="black"></MaterialIcon>
                                <span style={{
                                    fontSize: '2rem',
                                    fontWeight: 'bolder',
                                }}>
                                    START A CONVERSATION
                                </span>
                                <span style={{
                                    fontStyle: 'italic',
                                    fontSize: '1.2rem',
                                    fontWeight: 'lighter'
                                }}>Share your requirements and we'll get back to you with how we can help.</span>
                            </div>
                            <div className="form-container">
                                <ContactUsForm 
                                    handleNameChange={this.handleNameChange}
                                    handleEmailChange = {this.handleEmailChange}
                                    handleNumberChange = {this.handleNumberChange}
                                    handleDescriptionChange = {this.handleDescriptionChange}
                                    handleProjectTypeChange = {this.handleProjectTypeChange}
                                    submitInquiry = {this.submitInquiry}   
                                    names={this.state.names}
                                    emails={this.state.emails}
                                    descriptions={this.state.descriptions}
                                    phoneNumbers={this.state.phoneNumbers}
                                    projectTypes={this.state.projectTypes} 
                                />
                            </div>
                        </div>
                    </div>
                </div>
                
                <div className="footer-container" >
                    <MainFooter />
                </div>
            </div>
        )
    }
}