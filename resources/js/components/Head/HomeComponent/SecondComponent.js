import React, { Component } from 'react';
import '../../../../css/header/header.css';
import { FacebookLoginButton, GoogleLoginButton, TwitterLoginButton, InstagramLoginButton } from "react-social-login-buttons";
import { SocialIcon } from 'react-social-icons';
import Icon from 'react-geomicons';
import { Twitter, Location, Telephone, Message, Mail } from 'react-bytesize-icons';
import MaterialIcon, {colorPalette} from 'material-icons-react';
export default class SecondComponent extends Component {
    render() {
        return (
            <div className="fullPageSectionSecond">

                <div className="card-group-container">
                    <div style={{ display: 'flex', flexDirection: 'row', textAlign: 'center', alignItems: 'center', justifyContent: 'center' }}>
                        <h2>Services We Provide</h2>
                    </div>
                    <div class="card-group" >
                        <div class="card">
                            <div class="card-body text-center"  >
                                <img src={require('../../../../assest/image/Mobile-Application-Development.png')}
                                    style={{ width: '100%', height: 'auto' }}></img>
                            </div>
                        </div>
                        <div class="card ">
                            <div class="card-body">
                                <h2>Mobile Application Development</h2>
                                <h5>Our company boast of a team of dedicated App developers who help to provide App solutions and services that suits various business requirements and therefore, ensure good results.</h5>
                            </div>
                        </div>
                    </div>
                    <div class="card-group" style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap-reverse' }}>
                        <div class="card ">
                            <div class="card-body">
                                <h2>Mobile Application Development</h2>
                                <h5>Our company boast of a team of dedicated App developers who help to provide App solutions and services that suits various business requirements and therefore, ensure good results.</h5>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body text-center"  >
                                <img src={require('../../../../assest/image/webApplicationDevelopment.png')}
                                    style={{ width: '100%', height: 'auto' }}></img>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}