import React, { Component } from 'react';
import '../../../../css/header/header.css';
import { FacebookLoginButton, GoogleLoginButton, TwitterLoginButton, InstagramLoginButton } from "react-social-login-buttons";
import { SocialIcon } from 'react-social-icons';
import Icon from 'react-geomicons';
import { Twitter, Location, Telephone, Message, Mail } from 'react-bytesize-icons';
import MaterialIcon, {colorPalette} from 'material-icons-react';
export default class FirstComponent extends Component {
    render() {
        return (
            <div>
                <img id="main-image"
                    src={require('../../../../assest/image/mainBack.jpg')}></img>
                <div className="fullPageSectionFirst">
                    <div className="firstSectionText">
                        Accelerate Your
                        Digital Journey
                        with a full-service provider
                    </div>
                </div>
            </div>
        );
    }
}