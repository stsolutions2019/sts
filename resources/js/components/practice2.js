import React, { Component } from 'react';
import { Popover, PopoverHeader, PopoverBody } from 'reactstrap';

export default class PracticeTwo extends Component {
    constructor(props){
        super(props);
        this.togglePopover = this.togglePopover.bind(this);
        this.state={
            popoverOpen: false,
        }
    }
    togglePopover() {
        this.setState({
            popoverOpen: !this.state.popoverOpen,
        })
    }
    componentDidMount(){
        this.setState({
            popoverOpen: true
        })
    }
    render() {
        return (
            <div style={{ marginTop: '200px' }}>
                <button id="btnPop" onClick={this.togglePopover} color="primary">Click Me!</button>
                <Popover placement="top" isOpen={this.state.popoverOpen} target="btnPop" toggle={this.togglePopover}>
                    <PopoverHeader>Popover Title</PopoverHeader>
                    <PopoverBody>Sed posuere consectetur est at lobortis. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</PopoverBody>
                </Popover>
            </div>

        )
    }
}